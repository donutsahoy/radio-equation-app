//
//  DescriptionTabViewModel.swift
//  RadioEquationsApp
//
//  Created by Robert J. Sarvis Jr on 5/20/23.
//

import Foundation

struct DescriptionTabViewModel {
    let equation: Equation
}
