//
//  EquationScreenViewModel.swift
//  RadioEquationsApp
//
//  Created by Robert J. Sarvis Jr on 5/19/23.
//

import Foundation

struct EquationScreenTabBarViewModel {
    let equation: Equation
}
