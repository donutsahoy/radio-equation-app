//
// Created by Robert J. Sarvis Jr on 5/15/23.
//

import Foundation

protocol CalculationFunctionProtocol {
    func calculate(inputs: Double...) -> Double
}