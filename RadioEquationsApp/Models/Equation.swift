//
// Created by Robert J. Sarvis Jr on 5/15/23.
//

import Foundation

struct Equation: Codable {
    let title: String
    let description: String
    let id: EquationId
    let filters: [EquationFilterVariant]
}
