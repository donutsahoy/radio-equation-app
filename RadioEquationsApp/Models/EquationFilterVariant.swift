//
//  EquationFilterVariant.swift
//  RadioEquationsApp
//
//  Created by Robert J. Sarvis Jr on 7/22/23.
//

import Foundation

enum EquationFilterVariant: String, Codable {
    case advancedFunctions = "advancedFunctions"
}
