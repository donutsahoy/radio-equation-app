//
//  ObfuscateSupportEmail.swift
//  RadioEquationsApp
//
//  Created by Robert J. Sarvis Jr on 7/16/23.
//

import Foundation

private let supportPartOne = "incoming+donutsahoy-radio-equation-app-46014877-43alrzmo2th8bgszr6a0dh59a-issue"
private let supportPartTwo = "incoming.gitlab.com"



let supportEmail = "\(supportPartOne)@\(supportPartTwo)"
